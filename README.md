# Fractal visualization

## setup:

```sh 
$ virtualenv env
$ source env/bin/activate
$ pip install -r requirements.txt
```

## run:

```sh 
$ python main.py
```

## Explanation

On a given triangle, first put a random point in it. 
Then put another point in the middle of the last point and a random vertex. Repeat


Enjoy
