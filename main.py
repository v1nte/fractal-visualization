import random
import pygame
from pygame import draw



# WIDTH, HEIGHT = 1280, 720
WIDTH, HEIGHT = 900, 900
TICK = 360

TRIANGLE_SIDE_LENGTH = 500
POINT_RADIUS = 2
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()
is_running = True

def generate_random_point():
    x = random.randint(0, WIDTH - 1)
    y = random.randint(0, HEIGHT - 1)
    return (x, y)

def make_equilateral_triangle(x: int, y: int, side_length: int):
    # Calculate the height of the equilateral triangle
    height = (3**0.5 / 2) * side_length

    # Calculate the vertices of the triangle
    p1 = (x, y)
    p2 = (x + side_length, y)
    p3 = (x + side_length / 2, y - height)

    return [p1, p2, p3]


def draw_triangle(vertices: list):
    draw.polygon(screen, BLACK, vertices, 2)
    

def generate_random_point_in_triangle(vertex1: tuple, vertex2: tuple, vertex3: tuple):
    # Generate random barycentric coordinates
    alpha = random.uniform(0, 1)
    beta = random.uniform(0, 1 - alpha)
    gamma = 1 - alpha - beta

    # Interpolate a point using barycentric coordinates
    x = alpha * vertex1[0] + beta * vertex2[0] + gamma * vertex3[0]
    y = alpha * vertex1[1] + beta * vertex2[1] + gamma * vertex3[1]

    return (x, y)

def draw_point(point: tuple):
    x, y = point
    pygame.draw.circle(screen, BLACK, (x, y), POINT_RADIUS)


def calculate_middle_point(point1: tuple, point2: tuple):
    x1, y1 = point1
    x2, y2 = point2
    middle_x = (x1 + x2) // 2
    middle_y = (y1 + y2) // 2
    return (middle_x, middle_y)


points = []

TRIANGLE_POINTS = make_equilateral_triangle(WIDTH // 2 - TRIANGLE_SIDE_LENGTH // 2, HEIGHT // 2, TRIANGLE_SIDE_LENGTH)
points.append(generate_random_point_in_triangle(
     TRIANGLE_POINTS[0],
     TRIANGLE_POINTS[1],
     TRIANGLE_POINTS[2],
     ))

while is_running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            is_running = False

    screen.fill("purple")

    draw_triangle(TRIANGLE_POINTS)

    random_vertex = random.choice(TRIANGLE_POINTS)

    middle_point = calculate_middle_point(points[-1], random_vertex)

    points.append(middle_point)

    for p in points:
        draw_point(p)

    pygame.display.flip()

    clock.tick(TICK)

pygame.quit()
